#pragma once

#include <iostream>

#include "vec3f.hpp"

class vec4f{
    private:
        vec3f m_xyz;
        float m_w;

    public:

        vec4f(const vec3f& v, float w=0);

        vec4f(float x=0, float y=0, float z=0, float w=0);

        float getX() const;
        float getY() const;
        float getZ() const;
        float getW() const;
        vec3f getXYZ() const;

        void setX(float x);
        void setY(float y);
        void setZ(float z);
        void setW(float w);


        /**
         * Surcharge de l'opérator << pour afficher un vecteur sous la forme (x, y, z, w)
         * @os le flux recevant l'affichage du vecteur passé par référence
         * @v le vecteur à afficher passé par référence const
         * @return le flux passé en paramètre
         */
        friend std::ostream& operator<<(std::ostream& os, const vec4f& v);


};
