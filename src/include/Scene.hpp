#pragma once

#include "Object3D.hpp"
#include "mat4f.hpp"
#include "vec4f.hpp"

#include <vector>

class Scene{
    private:
        std::vector<Object3D*>* objects;
        std::vector<Scene*>* subscenes;
        mat4f* m_node_model;

        //std::vector<Light> light;
        
    public:
        //Constructeurs
        
        /**
         * Constructeur par défaut
         * Initialise les listes de Scene et d'Object3D vide
         */
        Scene();

        /**
         * Constructeur par recopie
         * @s la scene passée par référence à recopier
         * recopie l'intégralité des objets et des scenes
         */
        Scene(const Scene& s);

        /**
         * Destructeur de Scene
         * appelle également les destructeurs sur les listes de Scene et d'Object3D
         */
        ~Scene();

        //Getters, setters
        
        /**
         * Méthode qui permet de récupérer la matrice de transformation
         * du noeud de scene
         * @return La matrice à 4 dimensions contenant toutes les transformations à appliquer sur les sous noeuds et objets de la scène
         */
        mat4f& get_model_matrix() const;
        
        std::vector<Scene*>& get_subscenes() const;
        Scene& get_scene(int index) const;

        std::vector<Object3D*>& get_objects() const;
        Object3D& get_object(int index) const;

        /**
         * Permet de récupérer la liste des vertices de tous les objets et des sous scènes avec l'application des transformations de la matrice modele de la scène. Ils sont ajoutés à une liste passée en référence.
         * 
         */
        void get_transformed_vertices(std::vector<vec4f*>& trans_vertices);

        //Méthodes
        
        void add_subscene(Scene& scene);

        void remove_subscene(int index);

        void add_object(Object3D& object);

        void remove_object(int index);

        /**
         * Fonction permettant de bouger la scene dans l'espace d'après les coordonnées d'un vecteur. Cela bouge également les objets, sous-scenes contenues.
         * @param le vecteur de transition
         */
        void translate(const vec3f& v);

        /**
         * Fonction permettant de faire faire une rotation de la scene, des objet et sous scenes contenues sur l'axe x
         * @angle l'angle de rotation
         */
        void rotateX(float angle);

        /**
         * Fonction permettant de faire faire une rotation de la scene, des objet et sous scenes contenues sur l'axe y
         * @angle l'angle de rotation
         */
        void rotateY(float angle);
        
        /**
         * Fonction permettant de faire faire une rotation de la scene, des objet et sous scenes contenues sur l'axe z
         * @angle l'angle de rotation
         */
        void rotateZ(float angle);

        /**
         * Fonction qui applique une rotation selon un angle en radian et un axe donné à tous les objets de la scènes et aux sous scènes
         * @angle l'angle de rotation en radian
         * @axe un vecteur espace donnant les composantes de l'axe de rotation
         */
        void rotate(float angle, const vec3f& axe);


        //Opérateurs
        
        /**
         * Opérateur de recopie de la classe Scene. Recopie également les sous scenes et objets contenus.
         * @s la scene à recopier
         * @return la référence sur la scene après recopie
         */
        Scene& operator=(const Scene& s);
};
