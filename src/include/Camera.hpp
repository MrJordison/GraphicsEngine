#pragma once

#include "vec3f.hpp"
#include "mat4f.hpp"

/**
 * Classe modélisant la caméra.
 *
 */
class Camera{

    private:
        
        float m_near;
        float m_far;
        float m_aspect_ratio;
        float m_fov;

        vec3f* m_position;
        
        vec3f* m_look_pos;
        

    public:

        //Constructeur
        
        /**
         * Constructeur par défaut de Camera
         */
        Camera();

        /**
         * Constructeur de Camera, permet de lui passer des paramètres de bases à la création
         * position de base de la caméra un peu reculée sur l'axe z par rapport à l'orgine, qui est la direction regardée.
         * @near la distance proche max de rendu
         * @far la distance lointaine max de rendu
         * @angle l'angle de vision vertical pour le rendu
         * @aspect_ratio la taille du rendu (général width/height)
         */
        Camera(float near, float far, float angle, float aspect_ratio);

        /**
         * Constructeur par recopie de Camera
         * @c l'instance à recopier
         */
        Camera(const Camera& c);
        
        /**
         * Destructeur de Camera
         */
        ~Camera();


        //Getters, setters
        
        float get_near();
        float get_far();
        float get_fov();
        float get_aspect_ratio();
        vec3f& get_position();

        void set_near(float near);
        void set_far(float far);
        void set_fov(float angle);
        void set_aspect_ratio(float aspect_ratio);
        void set_position(const vec3f& v);

        //Fonctions

        /**
         * Fonction permettant de définir l'endroit où regarde la camera.
         * @v le vecteur position de l'endroit où la caméra va regarder
         */
        void look_at(const vec3f& v);

        /**
         * Fonction renvoyant la matrice de projection perspective via les sommets du frustrum
         */
        mat4f get_projection_matrix();

        /**
         * Fonction générant la matrice de view à partir de la caméra. Le vecteur représentant l'axe vertical de la camera est (0.,1.,0.)
         * @return la matrice générée
         */
        mat4f get_view_matrix();

};
