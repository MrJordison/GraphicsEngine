#pragma once

namespace utils{

    const float PI = 3.141592653589793238462643383279502884;

    bool compare_fp_gt(float f, float f2);
    bool compare_fp_lt(float f, float f2);
    bool compare_fp_eq(float f, float f2);
    int positive_mod(int a, int b);

}
