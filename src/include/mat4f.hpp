#pragma once

#include "vec4f.hpp"

class mat4f{

    private:
        float m_value[4][4];

    public:

        //Constructeurs
        mat4f();

        mat4f(float values[4][4]);

        //Getters, setters

        float get_value(int row_index, int column_index);

        void set_value(int row_index, int column_index, float value);

        //Méthodes
        
        static mat4f identity();

        static mat4f translate(const vec3f& v);

        static mat4f scale(float scalar);

        static mat4f scale(const vec4f& v);

        static mat4f rotate(float angle,const vec3f axe);

        static mat4f rotateX(float angle);

        static mat4f rotateY(float angle);

        static mat4f rotateZ(float angle);

        friend mat4f operator+(const mat4f& m, const mat4f& m2);

        mat4f& operator+=(const mat4f& m);

        friend mat4f operator-(const mat4f& m, const mat4f& m2);

        mat4f& operator-=(const mat4f& m);
        
        friend mat4f operator*(const mat4f& m, const mat4f& m2);
        
        mat4f& operator*=(const mat4f& m);

        friend vec4f operator*(const mat4f& m, const vec4f& v);

        friend std::ostream& operator<<(std::ostream& os, const mat4f& m);


};
