#pragma once

#include <iostream>

const float PI = 3.141592653589793238462643383279502884;
/**
 *Classe permettant de gérer des vecteurs(vecteurs à l'origine) dans l'espace. Contient les opérations de base sur des vecteurs (addition, soustraction, scale..)
 *
 */
class vec3f{
    private : 
        float m_x;
        float m_y;
        float m_z;

    public:
        /** Constructeur de vec3f
         * prend en paramètre les coordonnées en float x, y et z de l'instance à créer
         * Par défaut les coord sont initialisées à 0
         * @param x coordonnée en x
         * @param y coordonnée en y
         * @param z coordonnée en z
         *
         */
        vec3f(float x=0,float y=0,float z=0);

        //Getters,setters
        
        /**Permet de récupérer la coordonnée en x du point
         *@return la valeur sur l'axe x en float
         */
        float getX() const;
        /**Permet de récupérer la coordonnée en y du point
         *@return la valeur sur l'axe y en float
         */
        float getY() const;
        /**Permet de récupérer la coordonnée en z du point
         *@return la valeur sur l'axe z en float
         */
        float getZ() const;
        
        void setX(float x);
        void setY(float y);
        void setZ(float z);

        //Méthodes
        
        /**
         * Méthode qui permet de calculer la norme d'un vecteur
         * @return la norme du vecteur
         */
        float get_norme();

        /**
         * Méthode qui multiplie les coordonnées d'un vecteur par un scalaire
         * @scalar le scalaire
         * @return le vecteur multiplié par référence
         */
        vec3f scale(float scalar);

        /**
         * Méthode qui multiplie chaque coordonnée d'un vecteur par un scalaire
         * @s_x le scalaire pour la coordonnée x
         * @s_y le scalaire pour la coordonnée y
         * @s_z le scalaire pour la coordonnée z
         */
        vec3f scale(float s_x, float s_y, float s_z);

        /**
         * Méthode qui permet de faire faire une rotation au vecteur par rapport à l'axe des x
         * @angle la valeur de l'angle de rotation à faire en radian
         * @return le vecteur après rotation par référence
         */
        vec3f rotateX(float angle);

        /**
         * Méthode qui permet de faire faire une rotation au vecteur par rapport à l'axe des y
         * @angle la valeur de l'angle de rotation à faire en radian
         * @return le vecteur après rotation par référence
         */
        vec3f rotateY(float angle);

        /**
         * Méthode qui permet de faire faire une rotation au vecteur par rapport à l'axe des z
         * @angle la valeur de l'angle de rotation à faire en radian
         * @return le vecteur après rotation par référence
         */
        vec3f rotateZ(float angle);
        
        vec3f rotateXYZ(float angle_x, float angle_y, float angle_z);

        /**
         * Méthode qui calcule le produit scalaire entre deux vecteurs et retourne le résultat en float
         * @vec le vecteur avec lequel on effectue le calcul
         * @return la valeur du résultat en float
         */
        float dot_product(const vec3f& vec);

        /**
         * Méthode qui calcule le produit vectoriel entre deux vecteurs et retourne le vecteur orthogonal au plan formé par les opérandes
         * @vec le vecteur avec lequel on effectue le calcul
         * @return le vecteur orthogonal au plan formé par les opérandes
         */
        vec3f cross_product(const vec3f& vec);

        /**
         * Méthode qui permet de normaliser un vecteur tel que ses coordonnées x+y+z =1
         * @return le vecteur normalisé
         */
        vec3f normalize();
        


        //Opérateurs
        
        /**
         *Méthode qui génère un point à partir de l'addition de deux autres instance. L'addition consiste à ajouter les coordonnées de chaque axe entre elles.
         *@param p Le second point à additionner avec le premier depuis lequel la méthode est appelée
         *@return le nouveau point généré
         */
        friend vec3f operator+(const vec3f& v, const vec3f& v2);

        /**
         *Méthode d'addition identique à @sa operator+ , mais affecte directement les nouvelles coordonnées au premier point.
         *@return le nouveau point généré
         */
        vec3f operator+=(const vec3f& v);

        /**
         *Méthode qui génère un point à partir de la soustraction de deux autres instance. L'opération consiste à soustraire les coordonnées du premier point avec celle du second.
         *@param p Le second point à soustraire du premier depuis lequel la méthode est appelée
         *@return le nouveau point généré
         */
        friend vec3f operator-(const vec3f& v, const vec3f& v2);

        /**
         *Méthode de soustration identique à @sa operator- , mais affecte directement les nouvelles coordonnées au premier point.
         *@return le nouveau point généré
         */
        vec3f operator-=(const vec3f& v);

        /**
         * Méthode qui permet de savoir si deux vecteurs sont égaux, c'est à dire si leurs coordonnées x , y et z sont identiques
         * @v le premier vecteur à évaluer
         * @v2 le second vecteur à évaluer
         * @return true si x, y et z sont identiques, sinon false
         */
        friend bool operator==(const vec3f& v, const vec3f& v2);

        /**
         * Surcharge de l'opérator << pour afficher un vecteur sous la forme (x,y,z)
         * @os le flux recevant l'affichage du vecteur passé par référence
         * @v le vecteur à afficher passé par référence const
         * @return le flux passé en paramètre
         */
        friend std::ostream& operator<<(std::ostream& os, const vec3f& v);

};
