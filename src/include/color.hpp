#pragma once

#include "vec3i.hpp"

class color : public vec3i{
    private : 
        float m_alpha;

    public : 
        //Constructeur
        color();
        color(int r, int g, int b, float alpha = 1.);
        color(float r, float g, float b, float alpha = 1.);
        
    
        //Getters setters
        int getR() const;
        int getG() const;
        int getB() const;
        float getAlpha() const;

        void setR(int r);
        void setR(float r);
        void setG(int g);
        void setG(float g);
        void setB(int b);
        void setB(float b);
        void setAlpha(float alpha);

};

