#pragma once

#include "Camera.hpp"
#include "Scene.hpp"
#include "vec3i.hpp"
#include "color.hpp"

class Renderer{

    private :

        Camera* m_camera;
        Scene* m_main_scene;
        int m_width;
        int m_height;
        int z_buffer_size;

        void clip_cosuther();


    public:

        //Constructeurs
        Renderer(Camera& camera, Scene& scene, int width = 300, int height = 300);

        ~Renderer();

        //Getters, setters
        int get_width() const;
        int get_height() const;

        void set_width(int width);
        void set_height(int height);

        //Fonctions
        void render();

        int locate_region(vec3f& v);
        void clip_cosuther(int num_vert, std::vector<vec3f*>& vert);

        void rasterisation(std::vector<vec3i*>& vert,std::vector<std::vector<color*>>& frame);
        void rast_top_triangle(vec3i* top, vec3i* bot, vec3i* bot2, std::vector<std::vector<color*>>& frame, std::vector<std::vector<int>>& zbuffer);
        void rast_bottom_triangle(vec3i* bottom, vec3i* top, vec3i* top2, std::vector<std::vector<color*>>& frame, std::vector<std::vector<int>>& zbuffer);

        void display(std::vector<std::vector<color*>>& frame);


};
