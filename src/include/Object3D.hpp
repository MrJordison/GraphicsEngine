#pragma once

#include "vec3f.hpp"
#include "mat4f.hpp"
#include "vec4f.hpp"

#include <vector>

/**
 * Classe définissant un objet 3D dans l'espace. Se compose d'une liste de vertices.
 *
 */
class Object3D{
    
    private:
        std::vector<vec3f*>* m_vertices;
        mat4f* m_model_matrix;

        //std::vector<UV*> coord_tex;
        
        //std::vector<Texture *>
        
        //voir si besoin stockage normales

    public:

        //Constructeurs
        
        /**
         * Constructeur par défaut de la class Object3D
         * Initialize la liste de vertices vide
         */
        Object3D();

        /**
         * Constructeur par recopie
         * Recopie l'ensemble du tableau de vertices également
         */
        Object3D(const Object3D& o);

        /**
         * Constructeur de la class Object3D
         * prenant une liste de vertices en paramètre
         * @vertices la liste de vertices définissant le modèle de l'objet
         */
        Object3D(std::vector<vec3f*>& vertices);

        /**
         *Destructeur permettant de nettoyer le tableau de vertices alloué de l'objet
         */
        ~Object3D();


        //Getters, setters

        /**
         * Getter permettant de récupérer la liste des vertices de l'objet
         * @return reference sur la liste des vertices de l'objets
         */
        std::vector<vec3f*>& getVertices();

        /**
         * Getter pour récupérer la matrice modèle de l'objet
         * @return la référence de la matrice
         */
        mat4f& get_model_matrix() const;

        /**
         * Permet de récupérer la liste des vertices avec l'application des transformations de la matrice modele de l'objet. Ils sont ajoutés à une liste passée en référence.
         * @return la liste des vertices transformés 
         */
        void get_transformed_vertices(std::vector<vec4f*>& trans_vertices);

        //Méthodes
        
        /**
         * Fonction permettant de bouger l'objet dans l'espace d'après les coordonnées d'un vecteur
         * @param le vecteur de transition
         */
        void translate(const vec3f& v);

        /**
         * Fonction permettant de faire faire une rotation de l'objet
         * sur l'axe x
         * @angle l'angle de rotation
         */
        void rotateX(float angle);

        /**
         * Fonction permettant de faire faire une rotation de l'objet
         * sur l'axe y
         * @angle l'angle de rotation
         */
        void rotateY(float angle);

        /**
         * Fonction permettant de faire faire une rotation de l'objet
         * sur l'axe z
         * @angle l'angle de rotation
         */
        void rotateZ(float angle);
        
        /**
         * Fonction qui calcule la rotation de l'objet d'après un angle et un axe donné
         * @angle l'angle en radian de rotation
         * @axe un vecteur espace définissant l'axe de rotation de l'objet
         */
        void rotate(float angle, const vec3f& axe);
        
        /**
         * Fonction permettant d'appliquer une mise à l'échelle de l'objet selon des scalaires pour chaque axe de coordonnées
         * @s_x le scalaire pour l'axe des x
         * @s_y le scalaire pour l'axe des y
         * @s_z le scalaire pour l'axe des z
         */
        void scale(float s_x, float s_y, float s_z);

        /**
         * Fonction qui effectue une mise à l'échelle des 3 composantes de l'objet avec le même scalaire
         * @scalar le scalaire de mise à l'échelle
         */
        void scale(float scalar);


        //Opérateurs
        
        /**
         * Opérateur de recopie d'une instance d'Object3D. Recopie la liste des points également. 
         * @o l'objet à recopier
         * @return la référence de this après recopie
         */
        Object3D& operator=(const Object3D& o);

};
