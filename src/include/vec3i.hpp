#pragma once

#include <iostream>

class vec3i{

    protected : 
        int m_x;
        int m_y;
        int m_z;

    public :

        //Constructeurs
        vec3i(int x = 0, int y = 0, int z = 0);
        vec3i(int values[3]);

        //Getters, setters
        
        int getX() const;
        int getY() const;
        int getZ() const;

        void setX(int x);
        void setY(int y);
        void setZ(int z);

        //Méthodes
        
        friend bool operator==(const vec3i& v, const vec3i& v2);

        friend std::ostream& operator<<(std::ostream& os, const vec3i& v);
        

};
