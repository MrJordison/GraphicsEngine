#include "include/vec3f.hpp"

#include <iostream>
#include <cmath>

using namespace std;

//Constructeurs

vec3f::vec3f(float x,float y,float z):m_x(x),m_y(y),m_z(z){}

//Getters,setters

float vec3f::getX() const{return m_x;}
float vec3f::getY() const{return m_y;}
float vec3f::getZ() const{return m_z;}

void vec3f::setX(float x){m_x=x;}
void vec3f::setY(float y){m_y=y;}
void vec3f::setZ(float z){m_z=z;}

//Méthodes

float vec3f::get_norme(){
    return sqrt(pow(m_x,2.f) + pow(m_y,2.f) + pow(m_z,2.f));
}

vec3f vec3f::scale(float scalar){
    return vec3f(
        m_x * scalar,
        m_y * scalar,
        m_z * scalar
    );
}

vec3f vec3f::scale(float s_x, float s_y, float s_z){
    return vec3f(
        m_x * s_x,
        m_y * s_y,
        m_z * s_z
    );
}

vec3f vec3f::rotateX(float angle){
    return vec3f(
        m_x,
        ( cos(angle) * m_y ) + ( -sin(angle) * m_z ),
        ( sin(angle) * m_y ) + ( cos(angle) * m_z )
    );
}

vec3f vec3f::rotateY(float angle){
    return vec3f(
        ( cos(angle) * m_x ) + ( sin(angle) * m_z ),
        m_y,
        ( -sin(angle) * m_x ) + ( cos(angle) * m_z )
    );
}

vec3f vec3f::rotateZ(float angle){
    return vec3f(
        ( cos(angle) * m_x ) + ( -sin(angle) * m_y ),
        ( sin(angle) * m_x ) + ( cos(angle) * m_y ),
        m_z
    );
}

vec3f vec3f::rotateXYZ(float angle_x, float angle_y, float angle_z){
    cout << "Function not implemented for now, come back later.";
    return *this;
}

float vec3f::dot_product(const vec3f& vec){
    return (m_x * vec.m_x) + (m_y * vec.m_y) + (m_z * vec.m_z) ;
}

vec3f vec3f::cross_product(const vec3f& vec){
    return vec3f(
        (m_y * vec.m_z) - (m_z * vec.m_y),
        (m_x * vec.m_z) - (m_z * vec.m_x),
        (m_x * vec.m_y) - (m_y * vec.m_x)
    );
}

vec3f vec3f::normalize(){
    float norme  = 1 / this->get_norme();
    return vec3f(
        m_x * norme,
        m_y * norme,
        m_z * norme
    );
}

//Opérateurs

vec3f operator+(const vec3f& v, const vec3f& v2){
    return vec3f(
        v.m_x + v2.m_x,
        v.m_y + v2.m_y,
        v.m_z + v2.m_z
    );
}

vec3f vec3f::operator+=(const vec3f& v){
    m_x += v.m_x;
    m_y += v.m_y;
    m_z += v.m_z;

    return *this;
}

vec3f operator-(const vec3f& v, const vec3f& v2){
    return vec3f(
        v.m_x - v2.m_x,
        v.m_y - v2.m_y,
        v.m_z - v2.m_z
    );
}

vec3f vec3f::operator-=(const vec3f& v){
    m_x -= v.m_x;
    m_y -= v.m_y;
    m_z -= v.m_z;

    return *this;
}

bool operator==(const vec3f& v, const vec3f& v2){
    float epsilon = 0.00001;
    return (
        (v.m_x - v2.m_x < epsilon)
        && (v.m_y - v2.m_y < epsilon)
        && (v.m_z - v2.m_z < epsilon)
        );
}

ostream& operator<<(ostream& os, const vec3f& v){
    os << "(" << v.m_x << ", " << v.m_y << ", " << v.m_z << ")";
    return os;
}
