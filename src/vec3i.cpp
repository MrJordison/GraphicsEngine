#include "include/vec3i.hpp"

using namespace std;

//Constructeurs
vec3i::vec3i(int x, int y, int z) : m_x(x), m_y(y), m_z(z){}

vec3i::vec3i(int values[3]) : m_x(values[0]), m_y(values[1]), m_z(values[2]){}

//Getters, setters

int vec3i::getX() const{return m_x;}
int vec3i::getY() const{return m_y;}
int vec3i::getZ() const{ return m_z;}

void vec3i::setX(int x){ m_x = x;}
void vec3i::setY(int y){ m_y = y;}
void vec3i::setZ(int z){ m_z = z;}

//Méthodes

bool operator==(const vec3i& v, const vec3i& v2){
    return (v.m_x == v2.m_x) 
            && (v.m_y == v2.m_y)
            && (v.m_z == v2.m_z);
}

ostream& operator<<(ostream& os, const vec3i& v){
    os << "(" << v.m_x << ", " << v.m_y << ", " << v.m_z << ")";
    return os;
}

