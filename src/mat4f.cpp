#include "include/mat4f.hpp"

#include <cmath>

using namespace std;
	
//Constructeurs
mat4f::mat4f(){
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            m_value[i][j] = 0.f;
}

mat4f::mat4f(float values[4][4]){
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            m_value[i][j] = values[i][j];
}

//Getters, setters

float mat4f::get_value(int row_index, int column_index){
    if(row_index >=0 && row_index<4 && column_index>=0 && column_index<4)
        return m_value[row_index][column_index];
    else
        throw ("Erreur, index de colonne ou de ligne invalide");
}

void mat4f::set_value(int row_index, int column_index, float value){
    if(row_index >=0 && row_index<4 && column_index>=0 && column_index<4)
        m_value[row_index][column_index] = value;
    else
        throw ("Erreur, index de colonne ou de ligne invalide");
}

//Méthodes

mat4f mat4f::identity(){
    mat4f m;
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            m.m_value[i][j] = i==j ? 1.f : 0.f;
    return m;
}

mat4f mat4f::translate(const vec3f& v){
    mat4f m = identity();
    m.m_value[0][3] += v.getX();
    m.m_value[1][3] += v.getY();
    m.m_value[2][3] += v.getZ();
    return m;
}

mat4f mat4f::scale(float scalar){
    mat4f m = identity();
    m.m_value[0][0] *=scalar;
    m.m_value[1][1] *=scalar;
    m.m_value[2][2] *=scalar;
    return m;
}

mat4f mat4f::scale(const vec4f& v){
    mat4f m = identity();
    m.m_value[0][0] *=v.getX();
    m.m_value[1][1] *=v.getY();
    m.m_value[2][2] *=v.getZ();
    return m;
}

mat4f mat4f::rotate(float angle,const vec3f axe){
    mat4f m = identity();
    float c  = cos(angle);
    //float nc = 1 - c;
    float s = sin(angle);
    float x = axe.getX();
    float y = axe.getY();
    float z = axe.getZ();
    m.m_value[0][0] = (pow(x,2) * (1 - c)) + c;
    m.m_value[0][1] = (x * y * (1 - c)) - (z * s);
    m.m_value[0][2] = (x * z * (1 - c)) + (y * s);
    m.m_value[1][0] = (y * x * (1 - c)) + (z * s);
    m.m_value[1][1] = (pow(y,2.f) * (1 - c)) + c;
    m.m_value[1][2] = (y * z * (1 - c)) - (x * s);
    m.m_value[2][0] = (z * x * (1 - c)) - (y * s);
    m.m_value[2][1] = (z * y * (1 - c)) + (x * s);
    m.m_value[2][2] = (pow(z,2) * (1 - c)) + c;
    return m;
}

mat4f mat4f::rotateX(float angle){
    mat4f m = identity();
    m.m_value[1][1] = cos(angle) ;
    m.m_value[1][2] = -sin(angle);
    m.m_value[2][1] = sin(angle);
    m.m_value[2][2] = cos(angle);
    return m;
}

mat4f mat4f::rotateY(float angle){
    mat4f m = identity();
    m.m_value[0][0] = cos(angle);
    m.m_value[0][2] = sin(angle);
    m.m_value[2][0] = -sin(angle);
    m.m_value[2][2] = cos(angle);
    return m;
}

mat4f mat4f::rotateZ(float angle){
    mat4f m = identity();
    m.m_value[0][0] = cos(angle);
    m.m_value[0][1] = -sin(angle);
    m.m_value[1][0] = sin(angle);
    m.m_value[1][1] = cos(angle);
    return m;
}

mat4f operator+(const mat4f& m, const mat4f& m2){
    mat4f res = m;
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            res.m_value[i][j]+= m2.m_value[i][j];
    return res;
}

mat4f& mat4f::operator+=(const mat4f& m){
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            m_value[i][j]+= m.m_value[i][j];
    return *this;
}

mat4f operator-(const mat4f& m, const mat4f& m2){
    mat4f res = m;
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            res.m_value[i][j]-= m2.m_value[i][j];
    return res;
}

mat4f& mat4f::operator-=(const mat4f& m){
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            m_value[i][j]-= m.m_value[i][j];
    return *this;
}

mat4f operator*(const mat4f& m, const mat4f& m2){
    mat4f res;
    for(int i=0;i<4;++i)
        for(int j=0;j<4;++j)
            for(int k=0;k<4;++k)
                res.m_value[i][j] += m.m_value[i][k] * m2.m_value[j][k];
    return res;
}

mat4f& mat4f::operator*=(const mat4f& m){
    *this = *this * m;
    return *this;
}

vec4f operator*(const mat4f& m, const vec4f& v){
    vec4f res;
    res.setX(m.m_value[0][0] * v.getX() + m.m_value[0][1] * v.getY() + m.m_value[0][2] * v.getZ() + m.m_value[0][3] * v.getW());
    res.setY( m.m_value[1][0] * v.getX() + m.m_value[1][1] * v.getY() + m.m_value[1][2] * v.getZ() + m.m_value[1][3] * v.getW());
    res.setZ( m.m_value[2][0] * v.getX() + m.m_value[2][1] * v.getY() + m.m_value[2][2] * v.getZ() + m.m_value[2][3] * v.getW());
    res.setW( m.m_value[3][0] * v.getX() + m.m_value[3][1] * v.getY() + m.m_value[3][2] * v.getZ() + m.m_value[3][3] * v.getW());
    return res;
}

ostream& operator<<(ostream& os, const mat4f& m){

    for(int i = 0; i < 4; ++i){
        if(i == 0)
            os << "˹";
        else if (i == 3)
            os << "˻";
        else
            os << "|";
        for(int j = 0; j < 4; ++j){
            os << m.m_value[i][j];
            os << " ";
            if(j < 3)
            os << "|";
        
        }
        if(i == 0)
            os << "˺";
        else if (i == 3)
            os << "˼";
        else
            os << "|";
        os << endl;
    }

    return os;
}
