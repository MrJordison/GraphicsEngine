#include "include/Scene.hpp"

using namespace std;

//Constructeurs

Scene::Scene(){
    subscenes = new vector<Scene*>();
    objects = new vector<Object3D*>();
    m_node_model = new mat4f();
    *m_node_model = mat4f::identity();

}

Scene::Scene(const Scene& s){
    int size_obj = s.objects->size();
    int size_sub = s.subscenes->size();
    objects = new vector<Object3D*>(size_obj);
    subscenes = new vector<Scene*>(size_sub);
    int i;
    for(i=0;i<size_obj;++i)
        objects->at(i) = new Object3D(s.get_object(i));
    for(i=0;i<size_sub;++i)
        subscenes->at(i) = new Scene(s.get_scene(i));

    m_node_model = new mat4f(*s.m_node_model);
}

Scene::~Scene(){
    int i;
    for(i=0;i<objects->size();++i)
        delete objects->at(i);
    for(i=0;i<subscenes->size();++i)
        delete subscenes->at(i);
    delete objects;
    delete subscenes;
    delete m_node_model;
}

//Getters, setters

mat4f& Scene::get_model_matrix() const{
    return *m_node_model;
}

std::vector<Scene*>& Scene::get_subscenes() const{
    return *subscenes;
}

Scene& Scene::get_scene(int index) const{
    return *(subscenes->at(index));
}

std::vector<Object3D*>& Scene::get_objects() const{
    return *objects;
}

Object3D& Scene::get_object(int index) const{
    return *(objects->at(index));

}

void Scene::get_transformed_vertices(vector<vec4f*>& trans_vertices){
    int i;
    int begin = trans_vertices.size();

    //récupération des vertices des sous scènes
    for(i = 0; i < subscenes->size(); ++i)
        subscenes->at(i)->get_transformed_vertices(trans_vertices);

    //récupération des vertices des objets de la scènes
    for(i = 0; i < objects->size(); ++i)
        objects->at(i)->get_transformed_vertices(trans_vertices);

    //pour tous les vertices ajoutés depuis sous scènes et objets contenus
    for(i = begin; i < trans_vertices.size(); ++i){
        //les mettre à jour avec la matrice de transformation de la scene
        *trans_vertices[i] = *m_node_model * (*trans_vertices[i]);
    }
}


//Méthodes

void Scene::add_subscene(Scene& scene){
    subscenes->push_back(&scene);
}

void Scene::remove_subscene(int index){
    subscenes->erase(subscenes->begin()+index);
}

void Scene::add_object(Object3D& object){
    objects->push_back(&object);
}

void Scene::remove_object(int index){
    objects->erase(objects->begin()+index);
}

//Méthodes


void Scene::translate(const vec3f& v){
    *m_node_model = mat4f::translate(v) * *m_node_model;
}

void Scene::rotateX(float angle){
    *m_node_model = mat4f::rotateX(angle) * *m_node_model;
}

void Scene::rotateY(float angle){
    *m_node_model = mat4f::rotateY(angle) * *m_node_model;
}

void Scene::rotateZ(float angle){
    *m_node_model = mat4f::rotateZ(angle) * *m_node_model;
}

void Scene::rotate(float angle, const vec3f& axe){
    *m_node_model = mat4f::rotate(angle, axe) * *m_node_model;
}

Scene& Scene::operator=(const Scene& s){
    int i;
    for(i=0;i<objects->size();++i)
        delete objects->at(i);
    for(i=0;i<subscenes->size();++i)
        delete subscenes->at(i);
    delete objects;
    delete subscenes;
    delete m_node_model;

    int size_obj = s.objects->size();
    int size_sub = s.subscenes->size();
    objects = new vector<Object3D*>(size_obj);
    subscenes = new vector<Scene*>(size_sub);
    for(i=0;i<size_obj;++i)
        objects->at(i) = new Object3D(s.get_object(i));
    for(i=0;i<size_sub;++i)
        subscenes->at(i) = new Scene(s.get_scene(i));
    m_node_model = new mat4f(*s.m_node_model);
    return *this;
}
