#include "include/Renderer.hpp"
#include "include/vec4f.hpp"
#include "include/mat4f.hpp"
#include "include/utils.hpp"

#include "SDL2/SDL.h"


#include <vector>
#include <cmath>

using namespace std;

Renderer::Renderer(Camera& c,Scene& s, int width, int height){
    m_camera = &c;
    m_main_scene = &s;
    m_width = width;
    m_height = height;
    // par défaut mis à 16 bits
    z_buffer_size = 16;
}

Renderer::~Renderer(){
    delete m_camera;
    delete m_main_scene;
}

void Renderer::render(){
    // TRANSFORMATION WORLD ------------------------------------------------
    
    //récupération de tous les vertices de la scène principale avec application des transformations des matrices de modèle (vertices 4 dim, non homogènes)
    vector<vec4f*>* render_vert_nh= new vector<vec4f*>();
    m_main_scene->get_transformed_vertices(*render_vert_nh); 
    
    cout << "Vertices après récupération depuis la scène principale : " << endl << endl;
    for(int i = 0; i < render_vert_nh->size(); ++i)
        cout << "vertice n°" << i << " : " << *render_vert_nh->at(i) << endl;
    cout << endl;
    
    // TRANSFORMATION VIEW -------------------------------------------------
    
    //application de la matrice de vue de la caméra à chaque vertice
    mat4f view_matrix = m_camera->get_view_matrix();
    cout << "matrice de vue : " << endl << view_matrix << endl;
    for(int i = 0;i<render_vert_nh->size();++i)
        *render_vert_nh->at(i) = view_matrix * *render_vert_nh->at(i);


    cout << "Vertices après transformation par la matrice de vue : " << endl << endl;
    for(int i = 0; i < render_vert_nh->size(); ++i)
        cout << "vertice n°" << i << " : " << *render_vert_nh->at(i) << endl;
    cout << endl;
    
    // PROJECTION PERSPECTIVE ----------------------------------------------

    //http://rvirtual.free.fr/programmation/OpenGl/Sdin/c1107.htm
    //http://www.ogldev.org/www/tutorial12/tutorial12.html
    
    //On effectue la projection perspective de tous les vertices
    //Distance calculée de la camera jusqu'à la world window(projection plane)
    float dist_window = 1 / tan(m_camera->get_fov());
    vec4f temp;
    float invZ;
    mat4f projmat = m_camera->get_projection_matrix();
    cout << "Matrice de projection : \n" << projmat << endl;
    //Calcul des coordonnées dans le plan projection
    for(int i=0; i<render_vert_nh->size();++i){
        temp = *render_vert_nh->at(i);
        invZ = 1 / temp.getZ();
        //Assume que le vecteur créé a des coordonnées comprises en -1 et 1
        //*render_vert_nh->at(i) = vec4f( (temp.getX() * dist_window) / (invZ * m_camera->get_fov()) , (temp.getY() * dist_window) / invZ, dist_window, temp.getW());
        *(render_vert_nh->at(i)) = projmat * *(render_vert_nh->at(i));
    }

    cout << "Vertices après transformation par la matrice de projection : " << endl << endl;
    for(int i = 0; i < render_vert_nh->size(); ++i)
        cout << "vertice n°" << i << " : " << *render_vert_nh->at(i) << endl;
    cout << endl;

    // COORDONNEES NORMALISEES --------------------------------------------

    // Création d'un nouveau vecteur qui accueille tous les vertices après division par la coordonnée homogénique w
    
    float homogeneous;
    vector<vec3f*>* render_vert = new vector<vec3f*>(render_vert_nh->size());
    for(int i = 0; i < render_vert_nh->size(); ++i){
        homogeneous = 1 / render_vert_nh->at(i)->getW();
        render_vert->at(i) = new vec3f(render_vert_nh->at(i)->getXYZ());
        render_vert->at(i)->setX( render_vert->at(i)->getX() * homogeneous);
        render_vert->at(i)->setY( render_vert->at(i)->getY() * homogeneous);
        render_vert->at(i)->setZ( render_vert->at(i)->getZ() * homogeneous);
        
        //libération mémoire des vertices 4d
        delete render_vert_nh->at(i);
    }
    
    //libération de l'ancien vecteur
    delete render_vert_nh;
    
    cout << "Vertices après transformation homogénique : " << endl << endl;
    for(int i = 0; i < render_vert->size(); ++i)
        cout << "vertice n°" << i << " : " << *render_vert->at(i) << endl;
    cout << endl;

    // CLIPPING ------------------------------------------------------------
    
    //1st : enlever tout ce qui n'est pas dans le champ de vision
    //http://rvirtual.free.fr/programmation/OpenGl/Sdin/c1109.htm
    //http://rvirtual.free.fr/programmation/OpenGl/Sdin/c1108.htm
    
    cout <<"Passage des vertices à la moulinette du clipping" << endl << endl;
    //pour chaque triplet de vertices
    int i=0;
    while(i<render_vert->size()){
        cout << "Test pour le triplet à l'indice n°" << i << ", " << i + 1 << ", " << i + 2 << endl;
        int v1, v2, v3;
        v1 = locate_region(*render_vert->at(i));
        v2 = locate_region(*render_vert->at(i+1));
        v3 = locate_region(*render_vert->at(i+2));

        //si les trois vertices ne sont pas tous visibles
        if((v1 | v2 | v3) != 0){
            cout << "Tous les vertices ne sont pas visible dans le viewport" << endl;
            //test si triangle formé par le triplet de vertices, alors suppression du triplet
            if((v1 & v2)!=0 && (v1 & v3)!=0  && (v2 & v3)!=0){
                cout << "Aucun n'est visible, suppression du triplet" << endl;
                delete render_vert->at(i);
                delete render_vert->at(i+1);
                delete render_vert->at(i+2);
                render_vert->erase(render_vert->begin()+i, render_vert->begin()+i+3);
            }
            //sinon, besoin de clip le triplet
            else
                clip_cosuther(i,*render_vert);
        }
        else{
            cout << "triplet complètement visible |===> OK" << endl;
            i+=3;
        }
        cout << endl;
    }
    cout << "Fin de la partie de clipping" << endl;
    
    // VIEWPORT TRANSFORMATION ---------------------------------------------

    // conversion des vertices en coordonnées de pixels (avec gestion du z buffer)

    vec3f* buff;
    int x,y,z;
    int z_prof = pow(2,z_buffer_size) - 1;
    vector<vec3i*>* win_vert = new vector<vec3i*>();
    for(int i = 0; i < render_vert->size(); ++i){
        buff = render_vert->at(i);
        x = ((buff->getX() + 1) / 2) * (m_width - 1);
        y = (m_height - 1) - (((buff->getY() + 1) / 2) * (m_height - 1));
        z = ((buff->getZ() + 1) / 2) * (z_prof);
        
        //ajout du résultat au nouveau tableau
        win_vert->push_back(new vec3i(x,y,z));

        //libération mémoire du vertice précédent
        delete buff;
    }

    //libération de l'ancien vector
    delete render_vert;
    
    for(int i = 0; i < win_vert->size(); ++i)
        cout << "vertice n°" << i << " : " << *win_vert->at(i) << endl;

    // RASTERISATION -------------------------------------------------------
    
    //déclaration de la matrice de pixels de taille width * height qui sera envoyé à l'affichage
    vector<vector<color*>> frame;

    //remplissage par la méthode de rasterisation
    rasterisation(*win_vert,frame);

    // DISPLAY -------------------------------------------------------------

    display(frame);
}

int Renderer::locate_region(vec3f& v){
    //résultat du positionnement vertice stocké sur 6 bits(en pratique, on utilise un int auquel on ajoute des puissances successives de 2)
    int res = 0;
    //test si trop haut
    if(v.getY() > 1.)
        res |= 1;
    //si trop bas
    if(v.getY() < -1.)
        res |= 2;
    //si trop à gauche
    if(v.getX() < -1.)
        res |= 4;
    //si trop à droite
    if(v.getX() > 1.)
        res |= 8;
    //si trop près
    if(v.getZ() <- 1.)
        res |= 16;
    //si trop loin
    if(v.getZ() > 1.)
        res |= 32;

    //retourne le résultat cumulatif
    return res;
}

void Renderer::clip_cosuther(int num_vert, vector<vec3f*>& vert){
    //application clipping d'un polygone de cohen sutherland étendu en 3d
    
    //récupération du triplet de vertices dans un tableau temporaire
    vector<vec3f> polygon;
    polygon.push_back(*vert.at(num_vert));
    polygon.push_back(*vert.at(num_vert+1));
    polygon.push_back(*vert.at(num_vert+2));
    cout << "taille actuelle du polygone : " <<polygon.size() << endl;
    for(int i = 0; i< polygon.size(); ++i)
        cout << "vertice °" << i << " : " << polygon.at(i) << endl;
    
    //tests pour chaque plan de clipping
    float x, y, z, t;
    int b_index, a_index, size;
    int j=0;
    int i=1;
    bool modify;
    cout << "\n =!= Clipping des points =!=\n"<<endl;
    while(i < 7){
        modify = false;
        cout << "vertice n°" << j << " : " << polygon.at(j);
        //regarde quelle face doit être testée
        //clip des segments formé par le ce point et ses adjacents, qui est ensuite supprimé puis remplacé par les deux points d'intersections formés par ces segments et le plan concerné
        switch(i){
            case 1 :
                cout << " , test point trop à gauche : ";
                //cas point trop à gauche
                if(polygon.at(j).getX() < -1.){
                modify = true;
                cout << "ERR" << endl;
                    //récupération de la taille actuelle du nb de vertices du polygone // evite un nb d'appel du size() trop important
                    size = polygon.size();
                    if(j == 0 ){
                        b_index = size-1;
                        a_index = j+1;
                    }
                    else if(j == size-1){
                        b_index = j-1;
                        a_index = 0;
                    }
                    else{
                        b_index = j-1;
                        a_index = j+1;
                    }

                    x = -1.; 
                    //calcul du nouveau point d'intersection du segment [j+1; j]
                    t = abs(x - polygon.at(j).getX()) / abs(polygon.at(a_index).getX() - polygon.at(j).getX());
                    y = polygon.at(j).getY() + t * (polygon.at(a_index).getY() - polygon.at(j).getY());
                    z = polygon.at(j).getZ() + t * (polygon.at(a_index).getZ() - polygon.at(j).getZ());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    //calcul du nouveau point d'intersection du segment [j-1; j]
                    t = abs(x - polygon.at(j).getX()) / abs(polygon.at(b_index).getX() - polygon.at(j).getX());
                    y = polygon.at(j).getY() + t * (polygon.at(b_index).getY() - polygon.at(j).getY());
                    z = polygon.at(j).getZ() + t * (polygon.at(b_index).getZ() - polygon.at(j).getZ());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    polygon.erase(polygon.begin()+j);
                }
                else cout << " OK " << endl;
                break;

            case 2:
                // cas point trop à droite
                cout << " , test point trop à droite : ";
                if(polygon.at(j).getX() > 1.){
                    modify = true;
                    cout << "ERR" << endl;
                    //récupération de la taille actuelle du nb de vertices du polygone // evite un nb d'appel du size() trop important
                    size = polygon.size();
                    if(j == 0 ){
                        b_index = size-1;
                        a_index = j+1;
                    }
                    else if(j == size-1){
                        b_index = j-1;
                        a_index = 0;
                    }
                    else{
                        b_index = j-1;
                        a_index = j+1;
                    }

                    x = 1.; 
                    //calcul du nouveau point d'intersection du segment [j+1; j]
                    t = abs(x - polygon.at(j).getX()) / abs(polygon.at(a_index).getX() - polygon.at(j).getX());
                    cout << "point a_index : " << polygon.at(a_index) << endl;
                    cout << "t : " << t << endl;
                    y = polygon.at(j).getY() + t * (polygon.at(a_index).getY() - polygon.at(j).getY());
                    z = polygon.at(j).getZ() + t * (polygon.at(a_index).getZ() - polygon.at(j).getZ());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    //calcul du nouveau point d'intersection du segment [j-1; j]
                    t = abs(x - polygon.at(j).getX()) / abs(polygon.at(b_index).getX() - polygon.at(j).getX());
                    cout << "point b_index : " << polygon.at(b_index) << endl;
                    cout << "t : " << t << endl;
                    y = polygon.at(j).getY() + t * (polygon.at(b_index).getY() - polygon.at(j).getY());
                    z = polygon.at(j).getZ() + t * (polygon.at(b_index).getZ() - polygon.at(j).getZ());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    polygon.erase(polygon.begin()+j);
                   
                }
                else cout << "OK" << endl;
                break;

            case 3:
                //cas point trop en dessous
                cout << " , test point trop en dessous : ";
                if(polygon.at(j).getY() < -1.){
                    modify = true;
                    cout << "ERR" << endl;
                    //récupération de la taille actuelle du nb de vertices du polygone // evite un nb d'appel du size() trop important
                    size = polygon.size();
                    if(j == 0 ){
                        b_index = size-1;
                        a_index = j+1;
                    }
                    else if(j == size-1){
                        b_index = j-1;
                        a_index = 0;
                    }
                    else{
                        b_index = j-1;
                        a_index = j+1;
                    }

                    y = -1.; 
                    //calcul du nouveau point d'intersection du segment [j+1; j]
                    t = abs(y - polygon.at(j).getY()) / abs(polygon.at(a_index).getY() - polygon.at(j).getY());
                    x = polygon.at(j).getX() + t * (polygon.at(a_index).getX() - polygon.at(j).getX());
                    z = polygon.at(j).getZ() + t * (polygon.at(a_index).getZ() - polygon.at(j).getZ());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    //calcul du nouveau point d'intersection du segment [j-1; j]
                    t = abs(y - polygon.at(j).getY()) / abs(polygon.at(b_index).getY() - polygon.at(j).getY());
                    x = polygon.at(j).getX() + t * (polygon.at(b_index).getX() - polygon.at(j).getX());
                    z = polygon.at(j).getZ() + t * (polygon.at(b_index).getZ() - polygon.at(j).getZ());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    polygon.erase(polygon.begin()+j);
                
                }
                else cout << "OK" << endl;
                break;

            case 4:
                //cas point trop au dessus
                cout << " , test point trop au dessus : ";
                if(polygon.at(j).getY() > 1.){
                    modify = true;
                    cout << "ERR" << endl;
                    //récupération de la taille actuelle du nb de vertices du polygone // evite un nb d'appel du size() trop important
                    size = polygon.size();
                    if(j == 0 ){
                        b_index = size-1;
                        a_index = j+1;
                    }
                    else if(j == size-1){
                        b_index = j-1;
                        a_index = 0;
                    }
                    else{
                        b_index = j-1;
                        a_index = j+1;
                    }

                    y = 1.; 
                    //calcul du nouveau point d'intersection du segment [j+1; j]
                    t = abs(y - polygon.at(j).getY()) / abs(polygon.at(a_index).getY() - polygon.at(j).getY());
                    x = polygon.at(j).getX() + t * (polygon.at(a_index).getX() - polygon.at(j).getX());
                    z = polygon.at(j).getZ() + t * (polygon.at(a_index).getZ() - polygon.at(j).getZ());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    //calcul du nouveau point d'intersection du segment [j-1; j]
                    t = abs(y - polygon.at(j).getY()) / abs(polygon.at(b_index).getY() - polygon.at(j).getY());
                    x = polygon.at(j).getX() + t * (polygon.at(b_index).getX() - polygon.at(j).getX());
                    z = polygon.at(j).getZ() + t * (polygon.at(b_index).getZ() - polygon.at(j).getZ());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    polygon.erase(polygon.begin()+j);
                
                }
                cout << "OK" << endl;
                break;

            case 5:
                //cas point trop proche
                cout << " , test point trop proche : ";
                if(polygon.at(j).getZ() < -1){
                    modify = true;
                    cout << "ERR" << endl;
                    //récupération de la taille actuelle du nb de vertices du polygone // evite un nb d'appel du size() trop important
                    size = polygon.size();
                    if(j == 0 ){
                        b_index = size-1;
                        a_index = j+1;
                    }
                    else if(j == size-1){
                        b_index = j-1;
                        a_index = 0;
                    }
                    else{
                        b_index = j-1;
                        a_index = j+1;
                    }

                    z = -1; 
                    //calcul du nouveau point d'intersection du segment [j+1; j]
                    t = abs(z - polygon.at(j).getZ()) / abs(polygon.at(a_index).getZ() - polygon.at(j).getZ());
                    x = polygon.at(j).getX() + t * (polygon.at(a_index).getX() - polygon.at(j).getX());
                    y = polygon.at(j).getY() + t * (polygon.at(a_index).getY() - polygon.at(j).getY());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    //calcul du nouveau point d'intersection du segment [j-1; j]
                    t = abs(z - polygon.at(j).getZ()) / abs(polygon.at(b_index).getZ() - polygon.at(j).getZ());
                    x = polygon.at(j).getX() + t * (polygon.at(b_index).getX() - polygon.at(j).getX());
                    y = polygon.at(j).getY() + t * (polygon.at(b_index).getY() - polygon.at(j).getY());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    polygon.erase(polygon.begin()+j);
                }
                cout << "OK" << endl;
                break;

            case 6:
                //cas point trop loin
                cout << " , test point trop loin : ";
                if(polygon.at(j).getZ() > 1){
                    modify = true;
                    cout << "ERR" << endl;
                    //récupération de la taille actuelle du nb de vertices du polygone // evite un nb d'appel du size() trop important
                    size = polygon.size();
                    if(j == 0 ){
                        b_index = size-1;
                        a_index = j+1;
                    }
                    else if(j == size-1){
                        b_index = j-1;
                        a_index = 0;
                    }
                    else{
                        b_index = j-1;
                        a_index = j+1;
                    }

                    z = 1; 
                    //calcul du nouveau point d'intersection du segment [j+1; j]
                    t = abs(z - polygon.at(j).getZ()) / abs(polygon.at(a_index).getZ() - polygon.at(j).getZ());
                    x = polygon.at(j).getX() + t * (polygon.at(a_index).getX() - polygon.at(j).getX());
                    y = polygon.at(j).getY() + t * (polygon.at(a_index).getY() - polygon.at(j).getY());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    //calcul du nouveau point d'intersection du segment [j-1; j]
                    t = abs(z - polygon.at(j).getZ()) / abs(polygon.at(b_index).getZ() - polygon.at(j).getZ());
                    x = polygon.at(j).getX() + t * (polygon.at(b_index).getX() - polygon.at(j).getX());
                    y = polygon.at(j).getY() + t * (polygon.at(b_index).getY() - polygon.at(j).getY());
                    cout << "Nouveau point : " << vec3f(x,y,z) << endl;
                    polygon.insert(polygon.begin()+j+1, vec3f(x,y,z));
                    polygon.erase(polygon.begin()+j);
                }
                else cout << "OK" << endl;
                break;
        }
        if(!modify){
            if(j == (polygon.size() - 1)){
                ++i;
                cout << "\n";
                j = 0;
            }
            else ++j;
        }
    }
    cout << "Nouveau nombre de points : " << polygon.size() << endl;
    for(i = 0; i<polygon.size(); ++i)
        cout << polygon.at(i) << endl;

    //Le tableau temporaire forme à cet instant un polygone à n vertices (n>3)
    //pour ajouter ces vertices à la liste des triplets, il faut décomposer le polygone en triangles et ajouter les triplets de ces derniers
    
    //initialisation du tableau des nouveau triplets qui seront créés
    vector<vec3f*> new_triplets;
    int min = 0;
    
    //recherche du point ayant le y le plus petit du polygone ( ou si plusieurs même y, situé le plus à droite donc le x le plus grand) afin de déterminer le sens d'orientation du polygone 
    for(int i=1;i<polygon.size();++i){
        if(utils::compare_fp_lt(polygon.at(i).getY(), polygon.at(min).getY()))
            min = i;
        else if(utils::compare_fp_eq(polygon.at(min).getY(),polygon.at(i).getY()) && utils::compare_fp_gt(polygon.at(i).getX(), polygon.at(min).getX()))
            min = i;
    }

    
    //A partir du signe du produit vectoriel des segments formés par ce point et ses adjacents,on obtient un vecteur normal au plan du polygone et en effectuant un produit scalaire avec le point de vue de la camera, on peut savoir si le polygone est dans le sens horaire ou anti horaire
    vec3f bmin = polygon.at(utils::positive_mod(min-1,polygon.size())) - polygon.at(min);
    vec3f amin = polygon.at(utils::positive_mod(min+1,polygon.size())) - polygon.at(min);

    
    //dans un espace 3d, le produit vectoriel de deux vecteurs. retourne un vecteur normal au plan formé par ces derniers. Pour savoir connaitre l'orientation, il faut regarder le signe du determinant entre le résultat du prod vectoriel et la normale formée par le vectueur i et la position de la caméra.
    bool clockwise = !((bmin.getX()*amin.getY() - bmin.getY()*amin.getX()) > 0);
    cout << "sens anti horaire : " << clockwise << endl;
    cout << bmin << endl << amin << endl;

    vec3f s, s1, sp, normale;
    int dot00, dot01, dot02, dot11, dot12;
    float invDenom, u, v;
    i=0;
    int k, cpt, cpt2;
    bool areInside, oriented;
    
    cpt = 0;
    //Tant qu'il y a plus de trois vertices dans le polygone
    while(polygon.size()>3 && cpt < 10){

        s = polygon.at(utils::positive_mod(i - 1,polygon.size())) - polygon.at(utils::positive_mod(i, polygon.size()));
        s1 = polygon.at(utils::positive_mod(i + 1,polygon.size())) - polygon.at(utils::positive_mod(i, polygon.size()));

        cout << "s : " << polygon.at(i) << " " << polygon.at(utils::positive_mod(i - 1,polygon.size())) << " => " << s << endl;
        cout << "s1 : " << polygon.at(i) << " " << polygon.at(utils::positive_mod(i + 1,polygon.size())) << " => " << s1 << endl;
    
        //tests pour retirer une oreille
        
        normale = vec3f(s.getY(), -s.getX(), s.getZ());
        cout << (s1.getX()*normale.getX() + s1.getY() * normale.getY()) << " " <<s1.dot_product(normale)<< endl;

        //oriented = (s1.getX()*normale.getX() + s1.getY() * normale.getY()) > 0;
        oriented = s1.dot_product(normale) > 0;
        cout << "normale : " << normale << "\n orienté à l'intérieur : " << oriented << endl;

        if((oriented && clockwise) || (!oriented && !clockwise)){
            cout << "oreille potentielle vers l'intérieur" << endl;

            //Dans ce cas, on ne peut la supprimer du polygone que si aucun autre point n'est situé à l'intérieur de cette dernière
            k = utils::positive_mod(i + 2, polygon.size());
            dot00 = s.dot_product(s);
            dot01 = s.dot_product(s1);
            dot11 = s1.dot_product(s1);
            areInside=false;
            cpt2=0;
             
            //parcours de tous les autres points du polygone
            while(k != utils::positive_mod(i - 1, polygon.size()) && !areInside && cpt2<10){
                //Ici est utilisé la méthode du barycentre pour determiner la collision entre un point et un triangle
                sp = polygon.at(utils::positive_mod(k,polygon.size())) - polygon.at(utils::positive_mod(i, polygon.size()));
                dot02 = s.dot_product(sp);
                dot12 = s1.dot_product(sp);

                invDenom = 1.0 / (dot00 * dot11 - dot01 * dot01);
                u = (float)(dot11 * dot02 - dot01 * dot12) * invDenom;
                v = (float)(dot00 * dot12 - dot01 * dot02) * invDenom;
                areInside= areInside || ((utils::compare_fp_gt(u,0)|| utils::compare_fp_eq(u,0)) && (utils::compare_fp_gt(v,0) || utils::compare_fp_eq(v,0)) && (utils::compare_fp_lt(u + v,1.0)));
                k=utils::positive_mod(k+1,polygon.size());
                cpt2++;
            }
            //Si aucun n'est dedans, alors l'oreille peut être finalement retirée (cad le point i du tableau)
            if(!areInside){
                //ajout du triplet i-1, i et i+1 dans les triplets à display
                new_triplets.push_back(new vec3f(polygon.at(i)));
                new_triplets.push_back(new vec3f(polygon.at(utils::positive_mod(i - 1,polygon.size()))));
                new_triplets.push_back(new vec3f(polygon.at(utils::positive_mod(i + 1,polygon.size()))));
                cout << "suppression du point " << i << " " << polygon.at(i) << endl;

                polygon.erase(polygon.begin()+i);
                i = 0;
            }
            //Sinon, on passe au point suivant
            else{
                i = utils::positive_mod(i + 1,polygon.size()); 
            }
     
        }
        //Sinon, on passe au point suivant
        else{
            i = utils::positive_mod(i + 1,polygon.size()); 
        }
        cpt++;
    }
    //ajout du triplet restant dans le polygon
    new_triplets.push_back(new vec3f(polygon.at(0)));
    new_triplets.push_back(new vec3f(polygon.at(1)));
    new_triplets.push_back(new vec3f(polygon.at(2)));
    
    //suppression des 3 vertices initiales
    vert.erase(vert.begin() + num_vert,vert.begin() + num_vert + 3);
    
    //ajouts des nouveaux vertices créés au même emplacement
    vert.insert(vert.begin() + num_vert, new_triplets.begin(), new_triplets.end());

    cout << "fin de la triangulation " << endl;
    cout << "\nnouveaux triplets : " << endl;
    for (int i = 0; i< new_triplets.size(); ++i)
        cout << "vertice n°" << i << *new_triplets.at(i) << endl;
    
}

void Renderer::rasterisation(vector<vec3i*>& fragments, vector<vector<color*>>& frame){

    //création d'un zbuffer pour la frame en cours (tableau width * height de int)
    vector<vector<int>> zbuffer;
    for(int i = 0; i < m_height; ++i)
        zbuffer.push_back(vector<int>(m_width,pow(2,z_buffer_size)));

    for(int i = 0; i < m_height; ++i){
        frame.push_back(vector<color*>());
        for(int j = 0; j< m_width; ++j)
            frame.at(i).push_back(new color());
    }

    //itération pour chaque triplet de vertice
    int i = 0;

    while(i < fragments.size()){
        cout << "i : " << i << endl;
        
        //tri les sommets par y décroissant
        int swap;
        int tri[3] = {i, i+1, i+2};
        bool is_swap = false;
        int size = 3;
        while(!is_swap){
            is_swap = true;
            for(int k = 0;k<size-1;++k){
                if(fragments.at(tri[k+1])->getY() < fragments.at(tri[k])->getY()){
                    swap = tri[k+1];
                    tri[k+1] = tri[k];
                    tri[k] = swap;
                    is_swap = false;
                }
            }
        }
        //cas triviaux si les sommets tri[0] et tri[1] ont la même coordonnée y, c'est un flat top
        if(fragments.at(tri[0])->getY() == fragments.at(tri[1])->getY()){
            cout << "top " << endl;

            //change l'ordre du second et troisième vertice en paramètre selon leur coordonnée x
            if(fragments.at(tri[0])->getX() > fragments.at(tri[1])->getX())
                rast_bottom_triangle(fragments.at(tri[2]), fragments.at(tri[1]), fragments.at(tri[0]), frame, zbuffer);
            else
                rast_bottom_triangle(fragments.at(tri[2]), fragments.at(tri[0]), fragments.at(tri[1]), frame, zbuffer);
        }

        // si les sommets tri[1] et tri[2] ont la même coordonnée y, c'est un flat bottom
        else if(fragments.at(tri[1])->getY() == fragments.at(tri[2])->getY()){
            cout << "bottom" << endl;

            //change l'ordre du second et troisième vertice en paramètre selon leur coordonnée x
            if(fragments.at(tri[1])->getX() > fragments.at(tri[2])->getX())
                rast_top_triangle(fragments.at(tri[0]), fragments.at(tri[2]), fragments.at(tri[1]), frame, zbuffer);
            else
                rast_top_triangle(fragments.at(tri[0]), fragments.at(tri[1]), fragments.at(tri[2]), frame, zbuffer);
        }
        
        //sinon c'est un triangle quelconque, qui sera coupé en deux triangles horizontalement, au niveau du second vertice, et seront traités par les cas triviaux
        else{
            //sommets triés, la ligne de séparation des deux "flat" triangles se fera à partir du second point
            //et coupe le segment entre premier et troisième point
            
            //calcul des coordonnées x et z du point d'intersection entre la ligne de séparation et le segment
            int slice_x = fragments.at(tri[2])->getX() + ((float)(fragments.at(tri[1])->getY() - fragments.at(tri[2])->getY()) / (float)(fragments.at(tri[0])->getY() - fragments.at(tri[2])->getY())) * (fragments.at(tri[0])->getX() - fragments.at(tri[2])->getX()); 

            int slice_z = fragments.at(tri[2])->getZ() + ((float)(fragments.at(tri[1])->getY() - fragments.at(tri[2])->getY()) / (float)(fragments.at(tri[0])->getY() - fragments.at(tri[2])->getY())) * (fragments.at(tri[0])->getZ() - fragments.at(tri[2])->getZ()); 
            
            //création du nouveau point
            vec3i* slice_point = new vec3i(slice_x, fragments.at(tri[1])->getY(), slice_z);
            cout << "slice point : " << *slice_point << endl;

            //appel des fonctions de tracé des deux triangles formés par les 4 points
            if(fragments.at(tri[1])->getX() < slice_point->getX()){
                rast_bottom_triangle(fragments.at(tri[2]), fragments.at(tri[1]), slice_point, frame, zbuffer);
                rast_top_triangle(fragments.at(tri[0]), fragments.at(tri[1]), slice_point, frame, zbuffer);
            }
            else{
                rast_bottom_triangle(fragments.at(tri[2]), slice_point, fragments.at(tri[1]), frame, zbuffer);
                rast_top_triangle(fragments.at(tri[0]), slice_point, fragments.at(tri[1]), frame, zbuffer);
            }
        }

        i+=3;
    }



}

void Renderer::rast_top_triangle(vec3i* top, vec3i* bot, vec3i* bot2, vector<vector<color*>>& frame, vector<vector<int>>& zbuffer){

    //indice en x pour les début et fin de la ligne à tracer
    float xb_line = top->getX();
    float xe_line = top->getX();

    //calcul des delta à ajouter en x chaque itération pour dessiner une ligne
    float xdelta_end = (float)(bot2->getX() - top->getX()) / (float)(bot2->getY() - top->getY());
    float xdelta_begin = (float)(bot->getX() - top->getX()) / (float)(bot->getY() - top->getY());

    //On fait de même pour la coordonnées en z afin de mettre à jour le zbuffer
    float zb_line = top->getZ();
    float ze_line = top->getZ();

    //calcul des delta à ajouter en z chaque itération pour dessiner une ligne
    float zdelta_end = (float)(bot2->getZ() - top->getZ()) / (float)(bot2->getY() - top->getY());
    float zdelta_begin = (float)(bot->getZ() - top->getZ()) / (float)(bot->getY() - top->getY());

    int z_length, coord_z;
    float div_x_length, perc_x;
    //tracé d'autant de ligne qu'il y a de différence entre le haut et le bas du triangle
    for(int y_line = top->getY(); y_line <= bot->getY(); ++y_line){

        //ratio qui calculer le coefficient perc_x qui determinera la position du pixel en z
        div_x_length = 1 / (xe_line - xb_line);
        //longueur entre le z du pixel de début et le z du pixel de fin de ligne
        z_length = ze_line - zb_line;
        
        //itération pour chaque pixel de la ligne
        for(int x_col = xb_line; x_col <= xe_line; ++x_col){

            //calcul du coeff
            perc_x = (x_col - xb_line) * div_x_length;

            //determination de la coordonnée z
            coord_z = (z_length * perc_x) + zb_line;
            
            //si le pixel à dessiner est plus proche selon le zbuffer, on le met à jour ainsi que la couleur du pixel
            if(zbuffer.at(y_line).at(x_col) > coord_z){
                if(x_col==(int)xb_line || x_col==(int)xe_line || y_line==bot->getY())
                    (*frame.at(y_line).at(x_col)) = color(0,0,0);
                else
                    (*frame.at(y_line).at(x_col)) = color(0,230,0);
                zbuffer.at(y_line).at(x_col) = coord_z;
            }
        
        }

        //ajout du delta au coordonnées en x et z du début et fin de ligne
        xb_line += xdelta_begin;
        xe_line += xdelta_end;

        zb_line += zdelta_begin;
        ze_line += zdelta_end;
    }
}

void Renderer::rast_bottom_triangle(vec3i* bottom, vec3i* top, vec3i* top2, vector<vector<color*>>& frame, vector<vector<int>>& zbuffer){

    //indice en x pour les début et fin de la ligne à tracer
    float xb_line = bottom->getX();
    float xe_line = bottom->getX();

    //calcul des delta à ajouter en x chaque itération pour dessiner une ligne
    float xdelta_begin = (float)(top->getX() - bottom->getX()) / (float)(top->getY() - bottom->getY());
    float xdelta_end = (float)(top2->getX() - bottom->getX()) / (float)(top2->getY() - bottom->getY());

    //On fait de même pour la coordonnées en z afin de mettre à jour le zbuffer
    float zb_line = bottom->getZ();
    float ze_line = bottom->getZ();

    //calcul des delta à ajouter en z chaque itération pour dessiner une ligne
    float zdelta_begin = (float)(top->getZ() - bottom->getZ()) / (float)(top->getY() - bottom->getY());
    float zdelta_end = (float)(top2->getZ() - bottom->getZ()) / (float)(top2->getY() - bottom->getY());

    int z_length, coord_z;
    float div_x_length, perc_x;
    //tracé d'autant de ligne qu'il y a de différence entre le haut et le bas du triangle
    for(int y_line = bottom->getY(); y_line >= top->getY(); --y_line){

        //ratio qui calculer le coefficient perc_x qui determinera la position du pixel en z
        div_x_length = 1 / (xe_line - xb_line);
        //longueur entre le z du pixel de début et le z du pixel de fin de ligne
        z_length = (ze_line - zb_line);

        //itération pour chaque pixel de la ligne
        for(int x_col = xb_line; x_col <= xe_line; ++x_col){

            //calcul du coeff
            perc_x = (x_col - xb_line) * div_x_length;

            //determination de la coordonnée z
            coord_z = (z_length * perc_x) + zb_line;
            
            //si le pixel à dessiner est plus proche selon le zbuffer, on le met à jour ainsi que la couleur du pixel
            if(zbuffer.at(y_line).at(x_col) > coord_z){
                if(x_col==(int)xb_line || x_col==(int)xe_line || y_line==top->getY())
                    (*frame.at(y_line).at(x_col)) = color(0,0,0);
                else
                    (*frame.at(y_line).at(x_col)) = color(230,0,0);
                zbuffer.at(y_line).at(x_col) = coord_z;
            }
        
        }

        //ajout du delta au coordonnées en x et z du début et fin de ligne
        xb_line -= xdelta_begin;
        xe_line -= xdelta_end;

        zb_line -= zdelta_begin;
        ze_line -= zdelta_end;
    }

}

void Renderer::display(std::vector<std::vector<color*>>& frame){
    SDL_Event event;
    SDL_Renderer* renderer;
    SDL_Window* window;

    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(m_width, m_height, 0, &window, &renderer);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    for (int i = 0; i < frame.size(); ++i){
        for(int j = 0; j < frame.at(i).size(); ++j){
            //maj du rendererdrawcolor de SDL
            SDL_SetRenderDrawColor(renderer,
                    frame.at(i).at(j)->getR(),
                    frame.at(i).at(j)->getG(),
                    frame.at(i).at(j)->getB(),
                    255);
            //maj du pixel de l'image
            SDL_RenderDrawPoint(renderer,j,i);
        }
    }
    SDL_RenderPresent(renderer);
    while (1) {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

}
