#include "include/Camera.hpp"

#include <cmath>
#include <iostream>

using namespace std;

//Constructeurs

Camera::Camera(){
    m_near = 10;
    m_far = 10000;
    m_fov = PI/4;
    m_aspect_ratio = 16./9.f;
    m_position = new vec3f(0,0,-10);
    m_look_pos = new vec3f(0,0,0);
}

Camera::Camera(float near, float far, float angle, float aspect_ratio){
    m_near = near;
    m_far = far;
    m_fov = angle;
    m_aspect_ratio = aspect_ratio;
    m_position = new vec3f(0,0,-10);
    m_look_pos = new vec3f(0,0,0);
}


Camera::Camera(const Camera& c){
    m_near = c.m_near;
    m_far = c.m_far;
    m_fov = c.m_fov;
    m_aspect_ratio = c.m_aspect_ratio;
    m_position = c.m_position;
    m_look_pos = c.m_look_pos;
}

Camera::~Camera(){
    delete m_position;
    delete m_look_pos;
}

//Getters, setters
float Camera::get_near(){return m_near;}

float Camera::get_far(){return m_far;}
float Camera::get_fov(){return m_fov;}
float Camera::get_aspect_ratio(){return m_aspect_ratio;}
vec3f& Camera::get_position(){return *m_position;}

void Camera::set_near(float near){m_near = near;}

void Camera::set_far(float far){m_far = far;}

void Camera::set_fov(float angle){m_fov = angle;}

void Camera::set_aspect_ratio(float aspect_ratio){m_aspect_ratio = aspect_ratio;}

void Camera::set_position(const vec3f& v){
    if(m_position == nullptr)
        m_position = new vec3f(v);
    else
        *m_position = v;
}

void Camera::look_at(const vec3f& v){
    if(m_look_pos == nullptr)
        m_look_pos = new vec3f(v);
    else
        *m_look_pos = v;
}

mat4f Camera::get_projection_matrix(){
    //conversion degré en radian
    float degrad = PI / 180;
    float tangente = tan(m_fov * 0.5 * degrad);
    
    //calcul de la demi hauteur
    float height = m_near * tangente;

    //calcul de la demi largeur
    float width  = height / m_aspect_ratio;

    //http://www.songho.ca/opengl/gl_transform.html
    //generate frustrum volume left, right, bottom, top, near, far
    //frustrum(-width, width, -height, height,m_near, m_far)
    
    float z_div = 1 / (m_far - m_near);
    float x_div = 1 / (width - (-width));
    float y_div = 1 / (height - (-height));

    float proj_values[4][4] = {
        /*{(m_near / width), 0, 0, 0},
        {0, (m_near / height), 0, 0},
        {0, 0, (m_far + m_near) * z_div, (-(2 * m_far * m_near)) * z_div},
        {0, 0, 1, 0}*/
        {2 * m_near * x_div, 0, -((-width + width) * x_div), 0},
        {0, 2 * m_near * y_div, -((-height + height) * y_div), 0},
        {0, 0, (m_far + m_near) * z_div, -2 * m_near * m_far * z_div},
        {0, 0, 1, 0}
    };

    //création de la matrice de projection à partir du tableau statique
    mat4f res(proj_values);

    return res;
}

mat4f Camera::get_view_matrix(){
    //création des axes du repère de la camera
    vec3f zaxis = (*m_look_pos - *m_position).normalize();
    vec3f xaxis = (vec3f(0,1,0).cross_product(zaxis)).normalize();
    vec3f yaxis = xaxis.cross_product(zaxis);

    //valeurs pour la matrice view
    float view_values[4][4] = {
        {xaxis.getX(), xaxis.getY(),xaxis.getZ(),-(xaxis.dot_product(*m_position))},
        {yaxis.getX(), yaxis.getY(),yaxis.getZ(),-(yaxis.dot_product(*m_position))},
        {zaxis.getX(), zaxis.getY(),zaxis.getZ(),-(zaxis.dot_product(*m_position))},
        {0, 0, 0, 1}
    };
    
    return mat4f(view_values);
}
