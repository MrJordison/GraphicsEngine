#include "include/utils.hpp"

#include <cmath>

using namespace std;

namespace utils{
    const float epsilon = 0.000001f;

    bool compare_fp_lt(float f, float f2){
        return (f2 - f) > ((abs(f) < abs(f2) ? abs(f2) : abs(f)  ) * epsilon);
    }

    bool compare_fp_gt(float f, float f2){
        return (f - f2) > ((abs(f) > abs(f2) ? abs(f2) : abs(f) ) * epsilon);
    }

    bool compare_fp_eq(float f, float f2){
        return abs(f - f2) <= ((abs(f) > abs(f2) ? abs(f2) : abs(f) ) * epsilon);
    }

    int positive_mod(int a, int b){
        return ((a % b) + b) % b;
    }

}
