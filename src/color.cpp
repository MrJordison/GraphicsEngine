#include "include/color.hpp"

//Constructeur

color::color():vec3i(255,255,255), m_alpha(1.){}

color::color(int r, int g, int b, float alpha){
    setR(r);
    setG(g);
    setB(b);
    setAlpha(alpha);
}

color::color(float r, float g, float b , float alpha){
    setR(r);
    setG(g);
    setB(b);
    setAlpha(alpha);
}

//Getters setters
int color::getR() const{return m_x;}
int color::getG() const{return m_y;}
int color::getB() const{return m_z;}
float color::getAlpha() const{return m_alpha;}

void color::setR(int r){
    m_x = (r > 255) ? 255 : ((r < 0) ? 0 : r);
}

void color::setR(float r){
    int c = r * 255;
    m_x = (c > 255) ? 255 : ((c < 0) ? 0 : c);
}

void color::setG(int g){
    m_y = (g > 255) ? 255 : ((g < 0) ? 0 : g);
}

void color::setG(float g){
    int c = g * 255;
    m_y = (c > 255) ? 255 : ((c < 0) ? 0 : c);
}

void color::setB(int b){
    m_z = (b > 255) ? 255 : ((b < 0) ? 0 : b);
}

void color::setB(float b){
    int c = b * 255;
    m_z = (c > 255) ? 255 : ((c < 0) ? 0 : c);
}

void color::setAlpha(float alpha){
    m_alpha = (alpha > 1.) ? 1. : ((alpha < 0.) ? 0. : alpha);
}

