#include "include/vec4f.hpp"

using namespace std;

vec4f::vec4f(const vec3f& v, float w){
    m_xyz = v;
    m_w = w;
}

vec4f::vec4f(float x, float y, float z, float w){
    m_xyz = vec3f(x,y,z);
    m_w = w;
}

float vec4f::getX() const{
    return m_xyz.getX();
}

float vec4f::getY() const{
    return m_xyz.getY();
}

float vec4f::getZ() const{
    return m_xyz.getZ();
}

float vec4f::getW() const{
    return m_w;
}

vec3f vec4f::getXYZ() const{
    return m_xyz;
}

void vec4f::setX(float x){
    m_xyz.setX(x);
}

void vec4f::setY(float y){
    m_xyz.setY(y);
}

void vec4f::setZ(float z){
    m_xyz.setZ(z);
}

void vec4f::setW(float w){
    m_w = w;
}

ostream& operator<<(ostream& os, const vec4f& v){
    os << "(" << v.m_xyz.getX() << ", " << v.m_xyz.getY() << ", " << v.m_xyz.getZ() << ", " << v.m_w << ")";
    return os;
}

