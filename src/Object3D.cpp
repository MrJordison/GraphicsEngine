#include "include/Object3D.hpp"

#include <iostream>

using namespace std;

//Constructeurs

Object3D::Object3D(){
    m_vertices = new vector<vec3f*>();   
    m_model_matrix = new mat4f();
    *m_model_matrix = mat4f::identity();
}

Object3D::Object3D(const Object3D& o){
    int size=o.m_vertices->size();
    m_vertices = new vector<vec3f*>(size);
    for(int i=0;i<size;++i)
        m_vertices->at(i) = new vec3f(*o.m_vertices->at(i));

    m_model_matrix = new mat4f(*o.m_model_matrix);
}

Object3D::Object3D(vector<vec3f*>& vertices){
    //copie des vertices dans l'objets
    if(vertices.size()%3!=0)
        throw ("Un des triplet de vertices n'est pas complet");
    else m_vertices = &vertices;
    //intialisation de la matrice model identité
    m_model_matrix = new mat4f();
    *m_model_matrix = mat4f::identity();
}

Object3D::~Object3D(){
    //destruction de chaque vertice
    for(int i=0;i<m_vertices->size();++i)
        delete m_vertices->at(i);
    //destruction du tableau de vertices
    delete m_vertices;
    //destruction de la matrice model de l'objet
    delete m_model_matrix;
}


//Getters, setters

vector<vec3f*>& Object3D::getVertices(){
    return *m_vertices;
}

mat4f& Object3D::get_model_matrix() const{
    return *m_model_matrix;
}

void Object3D::get_transformed_vertices(vector<vec4f*>& trans_vertices){
    vec4f buffer;
    for(int i=0;i<m_vertices->size();++i){
        buffer = *m_model_matrix * vec4f(*m_vertices->at(i), 1.f);
        trans_vertices.push_back(new vec4f(buffer));
    }
}


//Fonctions

void Object3D::translate(const vec3f& v){
    *m_model_matrix = mat4f::translate(v) * *m_model_matrix;
}

void Object3D::rotateX(float angle){
    *m_model_matrix = mat4f::rotateX(angle) * *m_model_matrix;
}

void Object3D::rotateY(float angle){
    *m_model_matrix = mat4f::rotateY(angle) * *m_model_matrix;
}

void Object3D::rotateZ(float angle){
    *m_model_matrix = mat4f::rotateZ(angle) * *m_model_matrix;
}

void Object3D::rotate(float angle, const vec3f& axe){
    *m_model_matrix = mat4f::rotate(angle, axe) * *m_model_matrix;
}

void Object3D::scale(float s_x, float s_y, float s_z){
    *m_model_matrix = mat4f::scale(vec3f(s_x,s_y,s_z)) * *m_model_matrix;
}

void Object3D::scale(float scalar){
    *m_model_matrix = mat4f::scale(scalar) * *m_model_matrix;
}

Object3D& Object3D::operator=(const Object3D& o){
    for(int i=0;i<m_vertices->size();++i)
        delete m_vertices->at(i);
    delete m_vertices;
    delete m_model_matrix;

    int size=o.m_vertices->size();
    m_vertices = new vector<vec3f*>(size);
    for(int i=0;i<size;++i)
        m_vertices->at(i) = new vec3f(*o.m_vertices->at(i));
    m_model_matrix = new mat4f(*o.m_model_matrix);
    return *this;
}
