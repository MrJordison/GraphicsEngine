#include "src/include/vec3f.hpp"
#include "src/include/vec3i.hpp"
#include "src/include/vec4f.hpp"
#include "src/include/mat4f.hpp"
#include "src/include/color.hpp"
#include "src/include/Camera.hpp"
#include "src/include/Scene.hpp"
#include "src/include/Renderer.hpp"
#include "src/include/Object3D.hpp"

#include <iostream>

using namespace std;

int main(){
    //initialisation
    Camera* c = new Camera(1.,100.,60,16/9);
    Scene* scene = new Scene();
    c->set_position(vec3f(0,0,-1));
    Renderer* renderer = new Renderer(*c,*scene, 300, 300);

    vector<vec3f*>* vert = new vector<vec3f*>();
    //-z
    /*vert->push_back(new vec3f(0.,0.,0.));
    vert->push_back(new vec3f(0,1.,0.));
    vert->push_back(new vec3f(0.5,0.,-2.));*/
    //z
    /*vert->push_back(new vec3f(0,0,0.));
    vert->push_back(new vec3f(0,1,0));
    vert->push_back(new vec3f(0.5,0,2.));*/
    //x
    vert->push_back(new vec3f(0,0,0.));
    vert->push_back(new vec3f(0.5,0,0));
    vert->push_back(new vec3f(0,0.5,0));
    Object3D* o = new Object3D(*vert);
    scene->add_object(*o);

    //rendu 
    renderer->render();
    


    return 0;
}
