#include "../src/include/vec3f.hpp"
#include "gtest/gtest.h"
#include <iostream>

using namespace std;

class vec3fTest : public ::testing::Test{
    public : 
        vec3f * v;
        vec3f * v2;
    protected:
        virtual void SetUp(){
            v =new vec3f(4.f,2.2f,1.f);
            v2 = new vec3f(2.f,.5f,1.3f);
        }
        virtual void TearDown(){
            delete v;
            delete v2;
        }

};

TEST_F(vec3fTest,construct){
    EXPECT_FLOAT_EQ(v->getX(),4.f)<< "Erreur pour la coordonnée x";
    EXPECT_FLOAT_EQ(v->getY(),2.2f)<< "Erreur pour la coordonnée y";
    EXPECT_FLOAT_EQ(v->getZ(),1.f)<< "Erreur pour la coordonnée z";
}

TEST_F(vec3fTest,addition){
    vec3f v3 = *v + *v2;
    EXPECT_FLOAT_EQ(v3.getX(),6.f)<< "Erreur pour la coordonnée x";
    EXPECT_FLOAT_EQ(v3.getY(),2.7f)<< "Erreur pour la coordonnée y";
    EXPECT_FLOAT_EQ(v3.getZ(),2.3f)<< "Erreur pour la coordonnée z";
}

TEST_F(vec3fTest,soustraction){
    vec3f v3 = *v - *v2;
    EXPECT_FLOAT_EQ(v3.getX(),2.f)<< "Erreur pour la coordonnée x";
    EXPECT_FLOAT_EQ(v3.getY(),1.7f)<< "Erreur pour la coordonnée y";
    EXPECT_FLOAT_EQ(v3.getZ(),-0.3f)<< "Erreur pour la coordonnée z";
}

TEST_F(vec3fTest, norme_normalize){
    float norme = v->get_norme();
    EXPECT_FLOAT_EQ(norme,4.67332857f)<< "Erreur sur la valeur de la norme";
    vec3f normal = v->normalize();
    EXPECT_FLOAT_EQ(normal.getX(), v->getX()/norme)<< "Erreur pour x de la normale";
    EXPECT_FLOAT_EQ(normal.getY(), v->getY()/norme)<< "Erreur pour y de la normale";
    EXPECT_FLOAT_EQ(normal.getZ(), v->getZ()/norme)<< "Erreur pour z de la normale";
}

TEST_F(vec3fTest,scale){
    vec3f vscale = v->scale(2.f); 
    EXPECT_FLOAT_EQ(vscale.getX(),8.f)<< "Erreur pour la coordonnée x";
    EXPECT_FLOAT_EQ(vscale.getY(),4.4f)<< "Erreur pour la coordonnée y";
    EXPECT_FLOAT_EQ(vscale.getZ(),2.f)<< "Erreur pour la coordonnée z";

    vscale = v->scale(2.,1.,1.5f);
    EXPECT_FLOAT_EQ(vscale.getX(),8.f)<< "Erreur pour la coordonnée x";
    EXPECT_FLOAT_EQ(vscale.getY(),2.2f)<< "Erreur pour la coordonnée y";
    EXPECT_FLOAT_EQ(vscale.getZ(),1.5f)<< "Erreur pour la coordonnée z";
}

TEST_F(vec3fTest, rotate){
    vec3f rotate = vec3f(1.,1.,1.).rotateX(PI/2);
    EXPECT_FLOAT_EQ(rotate.getX(),1.f) << "erreur rotation coordonnée x";
    EXPECT_FLOAT_EQ(rotate.getY(),1.f) << "erreur rotation coordonnée y";
    EXPECT_FLOAT_EQ(rotate.getZ(),-1.f)<< "erreur rotation coordonnée y";
}


int main(int argc, char **argv){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
